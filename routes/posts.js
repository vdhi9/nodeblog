var express = require('express');
var router = express.Router();
var mongo = require('mongodb');
var monk = require('monk');
var db= monk('localhost/nodeblog');

router.get('/show/:id', function(req, res, next){
	var posts = db.get('posts');
	console.log('id '+req.params.id);
	/*posts.findById(req.params.id, function(err, post){
		res.render('show', {
		'post': post

		});
	});*/
	posts.findOne({_id: req.params.id}, function(err, post){
		if(err) throw err;
		res.render('show', {
		'post': post

		});
	});
	
});

router.get('/add', function(req, res, next){
	var categories = db.get('categories');
	categories.find({}, {}, function(err, categories){
		res.render('addpost', {
		'title':'Add Post',
		"categories": categories

		});
	});
	
});

router.post('/add', function(req, res, next){
	var title = req.body.title;
	var category = req.body.category;
	var body = req.body.body;
	var author = req.body.author;
	var date = new Date();

	if(req.files.mainimage){
		var mainImageOriginalName = req.files.mainimage.originalname;
		var mainImageName = req.files.mainimage.name;
		var mainImageMime = req.files.pmainimage.mimetype;
		var mainImagePath = req.files.mainimage.path;
		var mainImageExt = req.files.mainimage.extension;
		var mainImageSize = req.files.mainimage.size;
	}else{
		var mainImageName = 'noimage.png';
	}

	//form validation
	req.checkBody('title', 'Title feild is required').notEmpty();
	req.checkBody('body', 'Body feild is required').notEmpty();

	//check error
	var errors = req.validationErrors();

	if(errors){
		res.render('addpost', {
			errors:errors,
			'title':title,
			'body': body
		});
	}else{
		var posts = db.get('posts');
		//submit to db

		posts.insert({
			'title':title,
			'body':body,
			'category':category,
			'date':date,
			'author':author,
			'mainimage':mainImageOriginalName	
		}, function(error, post){
			if(error){
				res.send('there was an issue submitting the post');
			}else{
				req.flash('success', 'Post submitted');
				res.location('/');
				res.redirect('/');
			}
		});
	}
});


router.post('/addcomment', function(req, res, next){
	var name = req.body.name;
	var email = req.body.email;
	var body = req.body.body;
	var postid = req.body.postid;
	var commentdate = new Date();


	//form validation
	req.checkBody('name', 'Name feild is required').notEmpty();
	req.checkBody('body', 'Body feild is required').notEmpty();
	req.checkBody('email', 'Email feild is required').notEmpty();
	req.checkBody('email', 'enter valid email').isEmail();



	//check error
	var errors = req.validationErrors();

	if(errors){
		var posts = db.get('posts');
		posts.findById(postid, function(err, post){
			res.render('show', {
			'errors':errors,
			'post':post
		});
		});
		
	}else{
		var comment = {'name':name, 'emial':email, 'body':body, 'commentdate':commentdate};
		var posts = db.get('posts');

		posts.update({
			'_id':postid
		},
		{
			$push:{
				'comments':comment //array of comments.
			}
		}, 
		function(err, doc){
			if(err) {
				throw err;
			}
			else{
				req.flash('success', 'comment added');
				res.location('/posts/show/'+postid);
				res.redirect('/posts/show/'+postid);
			}
		});
	}
});

module.exports = router;
