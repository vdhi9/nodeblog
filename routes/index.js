var express = require('express');
var router = express.Router();
var mongo = require('mongodb');
const db = require('monk')('localhost/nodeblog');
//var monk = require('monk');
/*//var db = monk('localhost/nodeblog');
var mongoose = require('mongoose'); //driver which helps us to connect to mongdb
mongoose.connect('mongodb://localhost/nodeblog');
var db = mongoose.connection;
*/
/* GET home page. */
router.get('/', function(req, res, next) {
	
  //var db = req.db;
  
  if(!db)
  	console.log('connecttion error');
  var posts = db.get('posts');
 
  posts.find({},{}, function(err, posts){
  	
  	res.render('index', {
  		"posts": posts
  	});
 });
});

module.exports = router;
