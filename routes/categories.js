var express = require('express');
var flash = require('connect-flash');
var router = express.Router();
var mongo = require('mongodb');
const db = require('monk')('localhost/nodeblog');



router.get('/show/:category', function(req, res, next){
  var posts = db.get('posts');
  posts.find({category: req.params.category}, {}, function(err, posts){
      res.render('index',{
    'title':'req.params.category',
    'posts':posts
  });
  
  });
});


router.get('/add', function(req, res, next) {
	res.render('addcategory',{
    'title':'Add Category'
  });
  
});

router.post('/add', function(req, res, next) {
  var title = req.body.title;
  
  //form validation
  req.checkBody('title', 'Title feild is required').notEmpty();
 

  //check error
  var errors = req.validationErrors();

  if(errors){
    res.render('addcategory', {
      errors:errors,
      'title':title
      
    });
  }else{
    var categories = db.get('categories');
    //submit to db

    categories.insert({
      'title':title
      
    }, function(error, category){
      if(error){
        res.send('there was an issue saving the category');
      }else{
        req.flash('success', 'category added');
        res.location('/');
        res.redirect('/');
      }
    });
  }
  
});

module.exports = router;
